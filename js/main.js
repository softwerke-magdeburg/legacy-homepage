(function () {
    const header = document.getElementById("mainHeader");

    // the class no-transition disables the transition from header-background on load
    // after loading every change in header-background should have a transition
    // that's why we're removing the class here after a timeout
    setTimeout(() => header.classList.remove("no-transition"),300);

    const changeHeader = () => {
        const scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
        const value = document.querySelector(".hero.no-image") || scrollTop >= 50;
        header.classList.toggle("header-background", value);
    };

    let didScroll = false;

    window.addEventListener("scroll", () => {
        didScroll = true;
    });

    setInterval(() => {
        if (didScroll) {
            didScroll = false;
            changeHeader();
        }
    }, 100);

    changeHeader();

    for (const link of document.querySelectorAll("a[href*=\\#]")) {
        link.addEventListener("click", function (event) {
            if (this.pathname === window.location.pathname) {
                event.preventDefault();

                let top = 0;
                if(this.hash) {
                    const element = document.querySelector(this.hash);
                    if(element) {
                        const rect = element.getBoundingClientRect();
                        top = rect.top;
                    }
                }

                const baseUrl = window.location.href.split('#')[0];
                history.pushState({}, "", baseUrl + this.hash );

                window.scrollTo({ top, behavior: 'smooth' });
            }
        });
    }
})();
