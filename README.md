# Unsere Webseite
Wir verwenden eine [Jekyll](https://jekyllrb.com/)-basierte Webseite auf
Grundlage des [Frisco](https://github.com/CloudCannon/frisco-jekyll-template)-Templates.

## Build & Entwicklung
Jekyll ist ein [Static-Site-Generator](https://www.ionos.de/digitalguide/websites/webseiten-erstellen/was-ist-ein-static-site-generator/).
Das bedeutet, dass die Inhalte hier im Git-Repository angepasst werden
können; danach generiert Jekyll die statischen HTML-Dateien, die an den
Besucher ausgeliefert werden.

Um diesen Prozess zu vereinfachen, haben wir eine
[Docker](https://docs.docker.com/engine/)-Konfiguration hinzugefügt; die
Webseite kann also mit folgendem Befehl mit Docker Compose neu gebaut
werden, und ist danach auf http://localhost:8000 lokal verfügbar:

```bash
docker-compose up -d --build
```

Wenn man viele Änderungen schnell hintereinander machen will, kann es
sich auch lohnen, die Seite von Jekyll automatisch neu bauen zu lassen:

```bash
docker run --rm -it -p "8000:4000" -v "$PWD:/srv/jekyll" jekyll/builder sh -c 'gem install bundler:1.16.4 && bundle install && jekyll serve --safe'
```

## Aufgaben

- [x] Template anpassen
  - [x] ohne JavaScript funktionsfähig machen
  - [x] keine externen Ressourcen verwenden (außer Platzhalterbilder)
  - [x] Seiten ohne Bilder berücksichtigen
  - [x] Fußzeile anpassen
- [ ] Links zu sozialen Netzwerken
- [ ] Inhalte schreiben
- [ ] HowTo Website erstellen