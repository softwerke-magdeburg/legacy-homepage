FROM jekyll/builder AS build
RUN gem install bundler:1.16.4
COPY Gemfile Gemfile.lock ./
RUN bundle install
COPY . .
ENV JEKYLL_LOG_LEVEL debug
RUN bundle exec jekyll build && mv _site /tmp/output


FROM momar/web
COPY --from=build /tmp/output /var/www
ENV ENABLE_COMPRESSION 1
