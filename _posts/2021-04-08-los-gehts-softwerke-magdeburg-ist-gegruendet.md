---
title: "Los geht’s: Softwerke Magdeburg ist gegründet!"
date: 2021-05-14
description: TL;DR Wir haben den Verein Softwerke Magdeburg gegründet. Der Verein soll Online-Dienste zur Verfügung stellen, die Menschen helfen, ihre digitale Souveränität zurückzuerobern und damit eine freie Alternative zu den großen Internetmonopolen bieten. Es gibt noch viel zu tun. Seid mit dabei!
image: /images/header.jpg
author_staff_member: witziege
---

<br>

## Alles ganz neu: Die Softwerke gehen ans Netz

Es ist geschafft! Seit Ende Januar der ist Verein "Softwerke Magdeburg" offiziell gegründet. Fast ein Jahr hat es gedauert, um alle Voraussetzungen für eine erfolgreiche Gründung zu erfüllen. Nun geht es los und unsere Arbeit kann beginnen.

## Wir?!

Hinter dem Namen "Softwerke Magdeburg" steht eine kleine Gruppe Studis aus Magdeburg an der Elbe. Unter uns sind natürlich Informatik-Studis aus Magdeburg. Aber nicht ausschließlich! Wir setzen uns aus einem Team zusammen, das unterschiedliche Lieblingsthemen mitbringt. Von dem Interesse an alternativen Diensten über Kryptographie bis hin zum Thema Selbstbestimmung. Alle haben die Möglichkeit die eigenen Interessen und Kompetenzen mit einzubringen und den Verein mitzugestalten.


## Was ist los mit dem Internet?

Wir alle sind der Ansicht, dass das Internet ein integraler Bestandteil unseres Lebens ist. Vom Wetterbericht und Nachrichten, über Kunst und Kultur bis hin zu unserer intimsten Kommunikation. Alles findet online statt. Nur was dieses "online" heißt, ist vielen Menschen nicht klar. Große Internet-Firmen wie Google, Facebook, Microsoft und Apple bieten uns viele Dienste im Netz an und ermöglichen uns so eine komfortable und schnelle Möglichkeit, unsere täglichen Aufgaben in der online und offline Welt zu erledigen. Leider kommunizieren die wenigsten der großen Anbieter klar, wie ihre Dienste funktionieren und was eigentlich passiert, wenn z. B. eine Nachricht per Facebook oder Apple iMessage verschickt wird. Frage an dich: Was passiert denn eigentlich, wenn du eine Nachricht verschickst? Welche Vorgänge laufen auf deinem Smartphone oder deinem Computer ab und wo geht der Text oder das Bild eigentlich hin?

Wenn du keine wirkliche Antwort weißt, ist das nicht schlimm. Den meisten Menschen geht es so. Das Internet ist unsichtbar und unfassbar. Einen Brief kannst du sehen, anfassen und weißt, dass er mit einem Postauto zur Post geht. Bei einer WhatsApp-Nachricht ist das schon schwieriger nachzuvollziehen. Die Mehrheit der Menschen hatte noch nie ein richtiges Verständnis, was online passiert und wir als Menschheit verlieren so unsere Unabhängigkeit und unser digitales Leben gehört nicht mehr uns. Die Lösung ist allerdings super einfach: Du entscheidest dich, dein digitales Leben wieder selbst zu organisieren.


## Wie soll das denn bitte gehen?!

Ganz am Anfang steht dein Wille, wieder mehr Verantwortung für dich zu übernehmen. Das Internet ist viel größer als Google, Nachrichten können über so mehr Wege als nur WhatsApp verschickt werden und Dropbox besitzt nicht alleine sämtlichen Cloud-Speicher der Erde. Das Internet ist vielfältig und gehört allen Menschen und nicht nur den großen Konzernen. Was kannst du also tun, um wieder mehr Verantwortung zu übernehmen? Du entscheidest dich für eine Alternative.


## Jetzt kommen wir ins Spiel!

Der Verein Softwerke Magdeburg will eine dieser Alternativen sein. Wir möchten dir im Internet Programme und Dienste zur Verfügung stellen, die du nutzen kannst, um deinen Alltag zu bestreiten. Wir bieten Alternativen zu WhatsApp, iMessage, Facebook, Google Drive, Twitter, Instagramm und vielem mehr an. Für jeden Zweck ist was dabei. Und nicht nur das! Wir wollen dir auch noch erklären, wie unsere Angebote funktionieren. Nein, nein -- das wird kein Informatikstudium. Du wirst merken, dass ein kleines bisschen mehr Wissen über unser Online-Angebot dir dabei helfen wird, selbstbewusst und eigenständig sinnvolle Entscheidungen im Internet zu treffen. Wir zeigen Dir über deine Fotos, Videos, Nachrichten und allen anderen die Kontrolle zu behalten. Das ist in etwa so, wie etwas Grundwissen übers Autofahren. Du weißt vielleicht, dass du Benzin brauchst, damit der Motor läuft und das der Sprit verbrannt wird und dabei Abgase entstehen. Und so kannst du auch lernen, was alles passiert, wenn du ein Foto im Internet postest.

## Und warum sollte ich mich gerade hier anmelden?

Wir haben ein klar ausformuliertes [Mission-Statement](/). Darin beschreiben wir genau, worauf wir Wert legen. Besonders wichtig ist uns eine offene und ehrliche Kommunikation. Wir wollen dir genau vermitteln, was wir machen und was die von uns eingesetzte Software macht. Das besondere hierbei: Die von uns eingesetzte Software ist quelloffen und frei. (Wie bitte?) Nicht schlimm, wenn du noch nicht weißt, was das bedeutet. Kurzgesagt bedeutet es, dass die Programme, die wir nutzen, um alle Angebote umzusetzen, unter einer Lizenz veröffentlicht ist, die es uns ermöglicht zu checken, ob die Software mit unseren Werten übereinstimmt. Also ob sie z.B. deine Privatsphäre respektiert.

Zusätzlich haben wir uns auf die Fahne geschrieben, vor allem ein lokales und regionales Angebot zu schaffen. Ein bisschen wie saisonales Bio-Obst... der Vergleich hinkt ein bisschen, okay :) Das Internet ist heut zu Tage ein sehr zentral organisierter Ort. Viele Angebote, die du nutzt, gehören wahrscheinlich zu einem großen Konzern. Wusstest du, dass Instagramm und WhatsApp zu Facebook gehören? Also auch, wenn du dich vielleicht bewusst gegen ein Profil auf Facebook entschieden hast, vertraust du trotzdem deine Nachrichten auf WhatsApp der gleichen Firma an, weil WhatsApp seit einigen Jahren auch zu Facebook gehört. Damit wird das Internet zu einem Ort, der wenigen, sehr großen Firmen gehört und ihre Machtposition ausnutzen. Wenn das Internet ein freier Ort für alle Menschen bleiben soll, braucht es unabhängige Alternativen, die diesen Prozess entgegenwirken. Also wie die kleinen Bio-Bauern, die sich Nestlé entgegenstellen.
Wir verarbeiten Deine Daten nicht zu unseren Gunsten und du weißt genau, wo in Deutschland deine Daten liegen, quasi um die Ecke. So wie dein regionales Obst ;) Aber auch wenn unsere Dienste primär an Menschen in und um Magdeburg gerichtet sind, so steht unsere Tür selbstverständlich allen Menschen, egal wo sie herkommen, offen. Ach ja, und wir haben keine kommerziellen Absichten. Unser Zweck ist die Sache selbst und nicht der Profit.

## Also was jetzt?

Wir glauben, dass wir einen sinnvollen und wichtigen Beitrag im Internet leisten können. In den kommenden Wochen und Monaten werden wir unsere Website mit Artikeln und Beiträgen füllen und unsere ersten Online-Dienste starten. (Spoiler: Du wirst dich bald von WhatsApp und Twitter verabschieden können). Folge uns doch auf Mastodon oder aboniere diesen Blog! Wir bieten außerdem einen Newsletter. Wenn du magst, schreibe uns eine E-Mail bei Fragen oder Vorschlägen. Wir würden uns sehr freuen, mit dir in Kontakt zu treten und gemeinsam die Zukunft des Vereins und des Internets zu gestalten. 

Wir freuen uns über tatkräftige Unterstützung und vielleicht sogar neue Menschen im Verein ;)

<div class="button"><a href="/vereinsdokumente/Mitgliedsantrag.pdf">Jetzt Mitglied werden!</a></div>
