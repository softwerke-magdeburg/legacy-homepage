FROM jekyll/builder
RUN gem install bundler:1.16.4
RUN git clone https://codeberg.org/softwerke-magdeburg/website.git /input

WORKDIR /input
VOLUME /output
RUN bundle install

ADD https://codeberg.org/momar/webhook-receiver/raw/branch/master/webhook-receiver /usr/bin/webhook-receiver
RUN chmod +x /usr/bin/webhook-receiver
ENV WEBHOOK_PATH=/build
ENV WEBHOOK_QUEUE=kill
ENV JEKYLL_LOG_LEVEL debug
CMD git pull && bundle exec jekyll build -d /tmp/output && rsync -a --delete /tmp/output/ /output ;\
    exec /usr/bin/webhook-receiver sh -c 'git pull && bundle exec jekyll build -d /tmp/output && rsync -a --delete /tmp/output /output'
